#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2021, Dawid Potocki


print_usage() {
	cat <<EOF
Usage: $(basename "$0") [OPTIONS]

Options:
  -d         serve site locally (developement build)
  -p         build site for production

  -H <HOST>  host to bind to (default: 0.0.0.0)
  -P <PORT>  port to listen on (default: 4000)
  -q         silence output (kinda)
  -t         show full backtrace when error happens
  -V         verbose output
  -h         show this message
EOF
}

while getopts "dH:hP:pqtV" arg; do
	case "$arg" in
		d) opt_dev="1";;
		H) opt_host="--host=$OPTARG";;
		h) print_usage; exit 0;;
		P) opt_port="-P $OPTARG";;
		p) opt_prod="1";;
		q) opt_quiet="-q";;
		t) opt_trace="-t";;
		V) opt_verbose="-V";;
		*) exit 1;;
	esac
done


[ -n "$opt_prod" ] && [ -n "$opt_dev" ] && \
	printf "\033[1;31merror:\033[0m you can not build for production and developement at the same time\n" && exit 1
[ -n "$opt_quiet" ] && [ -n "$opt_verbose" ] && \
	printf "\033[1;31merror:\033[0m it is impossible to be verbose and quiet at the same time\n" && exit 1
[ -z "$opt_host" ] && opt_host="--host=0.0.0.0"
dev_flags="$opt_host $opt_port $opt_quiet $opt_trace $opt_verbose"
prod_flags="$opt_quiet $opt_trace $opt_verbose"


if [ -n "$opt_dev" ]; then
	# shellcheck disable=SC2086
	TZ=UTC bundle exec jekyll serve --livereload --incremental $dev_flags
elif [ -n "$opt_prod" ]; then
	# shellcheck disable=SC2086
	TZ=UTC JEKYLL_ENV=production NODE_ENV=production bundle exec jekyll build $prod_flags
else
	print_usage; exit 1
fi
