---
---

/*
 * Functions
 */

function sleep(ms) {
  /**
   * Sleep in milliseconds
   */
  return new Promise((resolve) => setTimeout(resolve, ms));
}


function fillMail(address, fillElement) {
  /**
   * Puts the email address in the element
   */
  document.querySelectorAll(`${fillElement}`).forEach((element) => {
    element.textContent = "";
    for (i = 0; i < address.length; i++) {
      element.innerHTML += `<span>${address[i]}</span><span class="d-none">--hi--<hr hidden /></span>`;
    }
  });
}


let canStartCopyAnimation = true;
function copyText(element, event) {
  /**
   * Copies text to clipboard and starts a popup animation
   */
  navigator.clipboard.writeText(element.textContent.replaceAll("--hi--", "")).then(
    async () => {
      if (canStartCopyAnimation === false) return;
      canStartCopyAnimation = false;
      document.querySelector("#clipboard").textContent = "Copied to clipboard!";
      document.querySelector("#clipboard").style.opacity = "100";
      document.querySelector("#clipboard").style.visibility= "visible";
      await sleep(3000);
      document.querySelector("#clipboard").style.opacity = "0"
      canStartCopyAnimation = true;
      await sleep(1000);
      if (canStartCopyAnimation === false) return;
      document.querySelector("#clipboard").style.visibility = "hidden";
    }
  );
}


/*
 * Main logic
 */

document.querySelectorAll(".onclick-copy").forEach((element) => {
  element.classList.add("a");
})

fillMail(
  [
    "{{ site.mail--personal[0] }}",
    "@",
    "{{ site.mail--personal[1] }}",
  ],
  ".mail--personal"
);

document.querySelectorAll(".onclick-copy").forEach((element) => {
  element.addEventListener("click", (event) => {
    copyText(element, event);
  });
  element.addEventListener("keydown", (event) => {
    if (event.key === "Enter") copyText(element, event);
  });
});


/*
 * Accessibility
 */

let navLinks = document.querySelector("#navbar__menu");
let hamburger = document.querySelector("#hamburger")

if (hamburger.checked === false)
  navLinks.setAttribute("aria-expanded", "false");
else navLinks.setAttribute("aria-expanded", "true");

hamburger.addEventListener("click", (e) => {
  if (navLinks.getAttribute("aria-expanded") === "false")
    navLinks.setAttribute("aria-expanded", "true")
  else navLinks.setAttribute("aria-expanded", "false");
});
