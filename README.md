# [dawidpotocki.com](https://dawidpotocki.com)

My _decent_ personal site.


## Setup

```sh
# Arch-based
$ sudo pacman -S git npm ruby ruby-bundler python-pygments
# Debian-based
$ sudo apt install git npm ruby bundler python3-pygments

$ git clone https://git.dawidpotocki.com/dawid/dawidpotocki.com
$ cd dawidpotocki.com
$ bundle
```

### Production

```sh
# Debian-based
$ sudo apt install nginx libnginx-mod-http-headers-more-filter
$ ln -s /var/www/dawidpotocki.com/nginx/000-main.conf /etc/nginx/sites-available/dawidpotocki.com
$ ln -s /etc/nginx/sites-available/dawidpotocki.com /etc/nginx/sites-enabled
```


## Build

```sh
$ ./build.sh -d  # Development
$ ./build.sh -p  # Production
```


## License

Project is licensed under [BSD-2-Clause](./LICENSE), unless specified otherwise.
